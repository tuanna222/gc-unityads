var hasNativeEvents = NATIVE && NATIVE.plugins && NATIVE.plugins.sendRequest;

var unityads = Class(function () {
	this.init = function () {
	}

	this.showInterstitial = function () {
		logger.log("{unityads} Showing interstitial");
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			logger.log("sendEvent unityads...");
			NATIVE.plugins.sendEvent("UnityAdPlugin", "showInterstitial",
				JSON.stringify({}));
		}
	};

	this.initBanner = function (position) {
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			NATIVE.plugins.sendEvent("UnityAdPlugin", "initBanner",
				JSON.stringify({position: position}));
		}
	};

	this.showBanner = function () {
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			NATIVE.plugins.sendEvent("UnityAdPlugin", "showBanner",
				JSON.stringify({}));
		}
	};

	this.hideBanner = function () {
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			NATIVE.plugins.sendEvent("UnityAdPlugin", "hideBanner",
				JSON.stringify({}));
		}
	};
});

exports = new unityads();
