package com.tealeaf.plugin.plugins;
import java.util.Map;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import com.tealeaf.EventQueue;
import com.tealeaf.TeaLeaf;
import com.tealeaf.logger;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import java.util.HashMap;

import com.tealeaf.plugin.IPlugin;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.FrameLayout;
import android.view.Gravity;
import android.view.View;


import com.tealeaf.EventQueue;
import com.tealeaf.event.*;

import com.unity3d.ads.android.UnityAds;
import com.unity3d.ads.android.IUnityAdsListener;

public class UnityAdPlugin implements IPlugin {
	public UnityAdPlugin() {
	}

	public void onCreateApplication(Context applicationContext) {
	}

	public void onCreate(Activity activity, Bundle savedInstanceState) {
        PackageManager manager = activity.getPackageManager();
        String unityAdId = "";
        try {
            Bundle meta = manager.getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA).metaData;
            if (meta != null) {
            	//old
                unityAdId = meta.getString("UNITY_AD_ID");
                
                if(!unityAdId.trim().equals("")){
                	UnityAds.init(activity, unityAdId, (IUnityAdsListener) this);
                	UnityAds.changeActivity(activity);
				}
            }
        } catch (Exception e) {
            android.util.Log.d("{UnityAds} EXCEPTION", "" + e.getMessage());
            e.printStackTrace();
        }
	}

	public void initBanner(final String json) {
		logger.log("{UnityAds} not implemented");
	}

	public void showBanner(final String json) {
		logger.log("{UnityAds} not implemented");
	}

	public void hideBanner(final String json) {
		logger.log("{UnityAds} not implemented");
	}

    public void showInterstitial(final String json){
        TeaLeaf.get().runOnUiThread(new Runnable()
        {
            public void run()
            {
                logger.log("{UnityAds} showInterstitial");
			    if(UnityAds.canShow()) {
				  	UnityAds.show();
				}
            }
        });
    }

	public void onResume() {
	}

	public void onStart() {
	}

	public void onPause() {
	}

	public void onStop() {
	}

	public void onDestroy() {
	}

	public void onNewIntent(Intent intent) {
	}

	public void setInstallReferrer(String referrer) {
	}

	public void onActivityResult(Integer request, Integer result, Intent data) {
	}

	public boolean consumeOnBackPressed() {
		return true;
	}

	public void onBackPressed() {
	}
}

