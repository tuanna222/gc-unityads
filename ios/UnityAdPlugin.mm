#import "UnityAdPlugin.h"

@implementation UnityAdPlugin
// The plugin must call super dealloc.
- (void) dealloc {
	[super dealloc];
}

// The plugin must call super init.
- (id) init {
	self = [super init];
	if (!self) {
		return nil;
	}
	return self;
}

 - (void) initializeWithManifest:(NSDictionary *)manifest appDelegate:(TeaLeafAppDelegate *)appDelegate {
 	@try {
 		NSLog(@"{unityads} Initialized with manifest");
 		self.tealeafViewController_ = appDelegate.tealeafViewController;
 		NSDictionary *ios = [manifest valueForKey:@"ios"];
 		NSString *unityAdId = [ios valueForKey:@"unityAdId"];
        [UnityAds initialize:unityAdId delegate:self];
        NSLog(@"{unityads} Initialized unityad with adID: %@", unityAdId);
 	}
 	@catch (NSException *exception) {
 		NSLog(@"{unityads} Failure to get ios:admobId from manifest file: %@", exception);
 	}
 }

- (void)unityAdsReady:(NSString *)placementId{
}

- (void)unityAdsDidError:(UnityAdsError)error withMessage:(NSString *)message{
}

- (void)unityAdsDidStart:(NSString *)placementId{
}

- (void)unityAdsDidFinish:(NSString *)placementId withFinishState:(UnityAdsFinishState)state{
}


 - (void) showInterstitial:(NSDictionary *)jsonObject {
 	@try {
 		NSLog(@"{unityads} Showing interstitial");
        if ([UnityAds isReady:@"video"]) {
            [UnityAds show:self.tealeafViewController_ placementId:@"video"];
        }
 	}
 	@catch (NSException *exception) {
 		NSLog(@"{unityads} Failure during interstitial: %@", exception);
 	}
 }

 - (void) initBanner:(NSDictionary *)jsonObject {
 	NSLog(@"{unityads} Not implementation");
 }

 - (void) showBanner:(NSDictionary *)jsonObject {
 	NSLog(@"{unityads} Not implementation");
 }

 - (void) hideBanner:(NSDictionary *)jsonObject {
 	NSLog(@"{unityads} Not implementation");
 }


@end

