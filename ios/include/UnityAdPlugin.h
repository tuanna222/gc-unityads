#import "PluginManager.h"
#import <UnityAds/UnityAds.h>

@interface UnityAdPlugin : GCPlugin <UnityAdsDelegate>

@property (nonatomic, retain) TeaLeafViewController *tealeafViewController_;

@end
